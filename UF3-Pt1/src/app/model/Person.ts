
import {DischargueDate} from "./DischargeDate"


export class Person{
    private _id: number;
    private _name: string;
    private _surname: string;
    private _email: string;
    private _phone: string;
    private _dischargeDate : DischargueDate;


	constructor(id: number, name: string, surname: string, email: string, phone: string, dischargeDate: DischargueDate) {
		this._id = id;
		this._name = name;
		this._surname = surname;
		this._email = email;
		this._phone = phone;
		this._dischargeDate = dischargeDate;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter surname
     * @return {string}
     */
	public get surname(): string {
		return this._surname;
	}

    /**
     * Getter email
     * @return {string}
     */
	public get email(): string {
		return this._email;
	}

    /**
     * Getter phone
     * @return {string}
     */
	public get phone(): string {
		return this._phone;
	}

    /**
     * Getter dischargeDate
     * @return {DischargueDate}
     */
	public get dischargeDate(): DischargueDate {
		return this._dischargeDate;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter surname
     * @param {string} value
     */
	public set surname(value: string) {
		this._surname = value;
	}

    /**
     * Setter email
     * @param {string} value
     */
	public set email(value: string) {
		this._email = value;
	}

    /**
     * Setter phone
     * @param {string} value
     */
	public set phone(value: string) {
		this._phone = value;
	}

    /**
     * Setter dischargeDate
     * @param {DischargueDate} value
     */
	public set dischargeDate(value: DischargueDate) {
		this._dischargeDate = value;
	}



	

}



	

